﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace strings_and_regular_expressions
{
    class Program
    {
        private static string imagesPattern = @"(?<image>(?:http|https).+\.(?:jpg|jpeg|png))";
        private static string splitPattern = @"\/([\w-]+\.(jpg|jpeg|png))";
        private static Uri uri = new Uri("https://www.oliverwicks.com/category/custom-suits/pref/collection:best-sellers/sort/newest");
        private static WebClient webClient = new WebClient();
        private static string[] imageUrlSegments;
        private static string webPageContent = string.Empty;
        private static int imageCounter = 1;

        static void Main(string[] args)
        {
            webPageContent = webReader(uri);

            IEnumerable<string> images = Regex.Matches(webPageContent, imagesPattern, RegexOptions.IgnoreCase).OfType<Match>()
                .Select(images => images.Groups["image"].Value).Distinct();

            foreach (string imageUrl in images)
            {
                imageUrlSegments = Regex.Split(imageUrl, splitPattern, RegexOptions.IgnoreCase);
                var pathDirectory = Directory.CreateDirectory(Environment.CurrentDirectory + "\\images");

                try
                {
                    webClient.DownloadFile(imageUrl, pathDirectory + "\\" + imageCounter++.ToString() + "_" + imageUrlSegments[1]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private static string webReader(Uri uri)
        {
            using (WebClient webClient = new WebClient())
            {
                Stream stream = webClient.OpenRead(uri);
                StreamReader reader = new StreamReader(stream);
                return reader.ReadToEnd();
            }
        }
    }
}